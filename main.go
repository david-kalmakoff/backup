package main

import "gitlab.com/david-kalmakoff/backup-helper/app"

func main() {
	app.Execute()
}
