package app

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "Backup",
	Short: "Backup is a tool for automating backups",
	Long:  "A useful tool to automate backups and automated old backup removal.",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Please check help for available commands")
	},
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
