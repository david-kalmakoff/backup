package app

import (
	"log/slog"

	"github.com/spf13/cobra"

	"gitlab.com/david-kalmakoff/backup-helper/business/backup"
)

func init() {
	rootCmd.AddCommand(backupCmd)
}

var backupCmd = &cobra.Command{
	Use:   "backup [dir to backup] [dir to backup to]",
	Short: "Backup Directories",
	Long:  "This will backup a set of directories to a compressed zip file with today's date as the name.",
	Args:  cobra.MinimumNArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		logger := slog.Default()

		count := len(args)

		backup.Run(logger, args[count-1], args[:count-1]...)
	},
}
