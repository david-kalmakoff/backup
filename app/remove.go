package app

import (
	"log/slog"

	"github.com/spf13/cobra"

	"gitlab.com/david-kalmakoff/backup-helper/business/remove"
)

var (
	Directory string
	Extension string
	Days      int
)

func init() {
	removeCmd.Flags().StringVarP(&Directory, "dir", "s", "", "Source directory of backups")
	removeCmd.Flags().StringVarP(&Extension, "ext", "e", "", "Extension of backup files")
	removeCmd.Flags().IntVarP(&Days, "days", "d", 0, "Days old to keep")

	removeCmd.MarkFlagRequired("dir")
	removeCmd.MarkFlagRequired("ext")
	removeCmd.MarkFlagRequired("days")

	rootCmd.AddCommand(removeCmd)
}

var removeCmd = &cobra.Command{
	Use:   "remove",
	Short: "Remove old backups",
	Long:  "This is remove the old backups in a given directory, given the amount of days that is considered old.",
	Run: func(cmd *cobra.Command, args []string) {
		logger := slog.Default()

		logger.Info("START REMOVE", "dir", Directory, "ext", Extension, "days", Days)

		if err := remove.Run(logger, Directory, Extension, Days); err != nil {
			logger.Error("could not complete run", "err", err)
			return
		}

		logger.Info("END REMOVE", "dir", Directory, "ext", Extension, "days", Days)
	},
}
