package backup

import (
	"archive/zip"
	"fmt"
	"io"
	"io/fs"
	"log/slog"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/david-kalmakoff/backup-helper/foundation/files"
)

func Run(logger *slog.Logger, destArg string, srcArgs ...string) {
	timeStart := time.Now()
	date := timeStart.Format(time.DateOnly)

	count := len(srcArgs)
	if count < 1 {
		logger.Error("not enough args", "count", count)
		return
	}

	logger.Info("START BACKUP", "src", srcArgs, "dest", destArg, "date", date)

	// ------------------------

	// Create temperary folder
	tmpDir, err := os.MkdirTemp("", "tmpDir")
	if err != nil {
		logger.Error("Creating tmpDir", "err", err)
		return
	}
	defer os.RemoveAll(tmpDir)

	backupDir := fmt.Sprintf("%v/%v", tmpDir, date)
	err = os.Mkdir(backupDir, 0755)
	if err != nil {
		logger.Error("creating backup folder", "err", err)
		return
	}

	// ------------------------

	// Copy all directories to temp
	for i, src := range srcArgs {
		if i == count-1 {
			continue
		}

		srcInfo, err := os.Stat(src)
		if err != nil {
			logger.Error("getting dir stats", "err", err)
			return
		}

		srcBackup := fmt.Sprintf("%v/%v", backupDir, srcInfo.Name())
		err = files.CopyDir(src, srcBackup)
		if err != nil {
			logger.Error("copying to tmpDir", "err", err)
			return
		}

		logger.Info(
			"copied to temp",
			"src",
			src,
			"size",
			float64(srcInfo.Size())/float64(int64(100)),
		)
	}

	// ------------------------

	// Compress temp directory
	logger.Info("compressing tmpDir")
	zipPath := fmt.Sprintf("%v/%v.zip", tmpDir, date)
	zipFile, err := os.Create(zipPath)

	zipWriter := zip.NewWriter(zipFile)

	err = filepath.Walk(backupDir, func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}
		file, err := os.Open(path)
		if err != nil {
			return err
		}
		defer file.Close()

		basePath := strings.Replace(path, tmpDir, "", 1)
		fileZipCopy, err := zipWriter.Create(basePath)
		if err != nil {
			return err
		}

		_, err = io.Copy(fileZipCopy, file)
		if err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		logger.Error("adding files to compressed", "err", err)
		return
	}

	zipStat, err := zipFile.Stat()
	if err != nil {
		logger.Error("getting zip stats", "err", err)
		return
	}

	zipWriter.Close()
	zipFile.Close()

	// ------------------------

	// Copy compressed file to destination
	logger.Info("copy to dest", "dir", destArg, "size", float64(zipStat.Size())/float64(int64(1e6)))
	zipDestPath := fmt.Sprintf("%v/%v.zip", destArg, date)
	err = files.CopyFile(zipPath, zipDestPath)
	if err != nil {
		logger.Error("copying compressed file to dest", "err", err)
		return
	}

	timeEnd := time.Now()
	timeDiff := timeEnd.Sub(timeStart)
	logger.Info("END BACKUP", "time", timeDiff)
}
