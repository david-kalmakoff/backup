package remove_test

import (
	"fmt"
	"log/slog"
	"os"
	"path/filepath"
	"slices"
	"strings"
	"testing"
	"time"

	"gitlab.com/david-kalmakoff/backup-helper/business/remove"
)

func TestRun(t *testing.T) {
	date := time.Now()
	total := 365

	tmp, err := os.MkdirTemp("", "tmp")
	if err != nil {
		panic(err)
	}
	defer os.RemoveAll(tmp)

	tests := []struct {
		name string
		dir  string
		days int
		err  bool
	}{
		{
			name: "errors from invalid days",
			dir:  tmp,
			days: 0,
			err:  true,
		},
		{
			name: "errors from empty dir",
			dir:  "",
			days: 7,
			err:  true,
		},
		{
			name: "errors from invalid dir",
			dir:  "/some-invalid-dir/here",
			days: 7,
			err:  true,
		},
		{
			name: "removes older than 7 days",
			dir:  tmp,
			days: 7,
		},
		{
			name: "removes older than 14 days",
			dir:  tmp,
			days: 14,
		},
		{
			name: "removes older than 30 days",
			dir:  tmp,
			days: 30,
		},
	}

	extension := ".zip"

	for _, test := range tests {
		tt := func(t *testing.T) {
			expected := make([]string, 0)
			for i := 0; i < total; i++ {
				offset := date.AddDate(0, 0, -i).Format(time.DateOnly)
				if i <= test.days {
					expected = append(expected, offset)
				}

				path := fmt.Sprintf("%v/%v%v", tmp, offset, extension)
				_, err := os.Create(path)
				if err != nil {
					panic(err)
				}

			}

			c, err := os.ReadDir(tmp)
			if err != nil {
				panic(err)
			}
			if len(c) != total {
				panic(fmt.Errorf("could not add files to tmp dir"))
			}

			logger := slog.Default()
			err = remove.Run(logger, test.dir, extension, test.days)
			if test.err && err == nil {
				t.Fatalf("\tF\t should error from invalid input")
			}
			if test.err && err != nil {
				t.Logf("\tP\t should error from invalid input")
				return
			}
			if err != nil {
				panic(err)
			}

			c, err = os.ReadDir(tmp)
			if err != nil {
				panic(err)
			}
			if len(c) != test.days+1 {
				t.Fatalf("\tF\t should remove files past '%d' days: %d", test.days, len(c))
			}
			t.Logf("\tP\t should remove files past '%d' days", test.days)

			err = filepath.Walk(test.dir, func(p string, info os.FileInfo, err error) error {
				if err != nil {
					return err
				}
				if info.IsDir() {
					return nil
				}

				filename := strings.Replace(p, test.dir+"/", "", 1)
				filename = strings.Replace(filename, extension, "", 1)

				if !slices.Contains(expected, filename) {
					return fmt.Errorf("should not include file: %s", filename)
				}

				return nil
			})
			if err != nil {
				panic(err)
			}

			err = os.RemoveAll(fmt.Sprintf("%v/*", tmp))
			if err != nil {
				t.Fatalf("\tF\t should remove expected files: %v", err)
			}
		}

		t.Run(test.name, tt)

	}
}
