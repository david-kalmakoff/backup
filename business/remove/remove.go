package remove

import (
	"fmt"
	"log/slog"
	"os"
	"path/filepath"
	"slices"
	"strings"
	"time"
)

var ignore = []string{".DS_Store"}

func Run(logger *slog.Logger, dir string, extension string, days int) error {
	y, m, d := time.Now().Date()
	date := time.Date(y, m, d, 0, 0, 0, 0, time.UTC)

	if dir == "" {
		return fmt.Errorf("dir cannot be empty")
	}
	if days < 1 {
		return fmt.Errorf("days cannot be less than 1; %d", days)
	}

	path := filepath.Clean(dir)
	stat, err := os.Stat(path)
	if err != nil {
		return err
	}
	if !stat.IsDir() {
		return fmt.Errorf("not a directory")
	}

	removed := make([]string, 0)

	err = filepath.Walk(path,
		func(p string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if info.IsDir() {
				return nil
			}

			filename := strings.Replace(p, path+"/", "", 1)
			filename = strings.Replace(filename, extension, "", 1)

			if slices.Contains(ignore, filename) {
				return nil
			}

			t, err := time.Parse(time.DateOnly, filename)
			if err != nil {
				return err
			}

			diff := date.Sub(t)
			if diff.Hours()/24 <= float64(int64(days)) {
				return nil
			}

			removed = append(removed, filename)

			err = os.Remove(p)
			if err != nil {
				return err
			}

			return nil
		})
	if err != nil {
		return err
	}

	if len(removed) > 0 {
		logger.Info("deleted old backups", "count", len(removed), "removed", removed)
	}

	return nil
}
