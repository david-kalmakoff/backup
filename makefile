dev-backup:
	go run app/backup-directories/main.go data/src data/src1 data/src2 data/dest

dev-remove:
	go run app/remove-old/main.go -dir=data/dest -ext=.zip -days=1

build:
	go build .

test:
	go test ./...

install:
	go install .
