# Automated Backups

## Install

```bash
go install gitlab.com/david-kalmakoff/backup-helper@latest
```

## Build

```bash
make build
```

## Run Backup

### Arguments

The application takes in an array of directories. The last directory is the destination directory.

```bash
backup-helper backup /src1 /src2 /dest
```

## Run Remove Old Backups

### Flags

- `dir` `s` - This is the directory to check for old backups in
  - It is expected that files are named with a datestamp i.e. `2020-10-05.zip`
- `ext` `e` - File extension of the backup files
- `days` `d` - Number of days to keep, removing any older

```bash
build-helper remove -s /some/dir -e .zip -d 7
```




